#ifndef SLIDING_SOLVER_H_
#define SLIDING_SOLVER_H_

#include "Board_Tile.h"
#include <string>
#include <queue>
#include <vector>

using namespace std;

struct Compare
{
	bool operator()(const Board_Tile &bt1, Board_Tile &bt2) const
	{
		return bt1.DValue > bt2.DValue;
	}
};

class Sliding_Solver {
public:
	Sliding_Solver(const string& startConfigStr, const string& goalConfigStr);
	~Sliding_Solver();
	void Solve_Puzzle();
	void printResult(const Board_Tile& toPrint);

private:
	Board_Tile* startConfig;
	Board_Tile* goalConfig;
	vector<int> D_values;
	priority_queue<Board_Tile,vector<Board_Tile>, Compare> pq;
	vector<Board_Tile*> allConfigs;
	void resolveDuplicate();
	void clearQueue();
};

#endif /* SLIDING_SOLVER_H_ */