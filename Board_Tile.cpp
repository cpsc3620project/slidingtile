#include "Board_Tile.h"
#include <iostream>

using namespace std;

Board_Tile::Board_Tile(const string& input)
{
	this -> config = input;
}

Board_Tile::~Board_Tile() 
{

}

vector<Board_Tile*> Board_Tile::nextConfigs()
{
  vector<Board_Tile*> nextConfigs;

	getNewConfig(this->config);

  for(int i = 0; i < this->tempNextMoves.size(); i++)
  {
    nextConfigs.push_back(new Board_Tile(tempNextMoves[i]));
  }

	return nextConfigs;
}

int Board_Tile::numMoves()
{
	return this->numOfMoves;
}

int Board_Tile::Manhattan_Distance(const Board_Tile& goalConfig)
{
  int distance = 0;

  int currX = 0;
  int currY = 0;
  int goalX = 0;
  int goalY = 0;

  int tempX = 0;
  int tempY = 0;
  int tempAns = 0;

  string strGoal = goalConfig.toString();

  for(char& c : strGoal) 
  {
    if(c != '0')
    {
      tuple<int, int> goal = getCoordinate(strGoal, c);
      goalX = get<0>(goal);
      goalY = get<1>(goal);

      tuple<int, int> curr = getCoordinate(this->config, c);
      currX = get<0>(curr);
      currY = get<1>(curr);

      tempX = abs(currX - goalX);
      tempY = abs(currY - goalY);
      tempAns = tempX + tempY;

      distance += tempAns;
    }
  }

  cout << "Manhattan_Distance: " << distance << endl;

	return distance;
}

string Board_Tile::getNewConfig(string config)
{
	string newConfig = "";

	int row = 3;
	int col = 3;

	char matrix [3][3];

  int pos_x, pos_y;

    int inc = 0;
    for (int n = 0; n < row; n++)
  	{
  		for (int m = 0; m < col; m++)
    	{
    		matrix[n][m]=config[inc];
      		if(config[inc] == '0')
      		{
      			pos_y = m;
      			pos_x = n;
      		}
      		inc++;
    	}
  	}

  	for (int i = 0; i < row; ++i)
    {
        for (int j = 0; j < col; ++j)
        {
        	cout << matrix[i][j] << ' ';
        }
       	cout << endl;
    }

    cout << pos_x << ' ';
  	cout << pos_y << endl;

  	if (pos_x == 0)
  	{
  		if (pos_y == 0)
  		{
  			swapTile(matrix,pos_x, pos_y, pos_x+1, pos_y);
        incrementMove("R");
        swapTile(matrix,pos_x, pos_y, pos_x, pos_y+1);
        incrementMove("D");
  		}
      else if (pos_y == 2)
      {
        swapTile(matrix,pos_x, pos_y, pos_x, pos_y-1);
        incrementMove("U");
        swapTile(matrix,pos_x, pos_y, pos_x+1, pos_y);
        incrementMove("R");
      }
  		else if (pos_y == 1)
  		{
        swapTile(matrix,pos_x, pos_y, pos_x+1, pos_y);
        incrementMove("R");
        swapTile(matrix,pos_x, pos_y, pos_x, pos_y+1);
        incrementMove("D");
  			swapTile(matrix,pos_x, pos_y, pos_x, pos_y-1);
        incrementMove("U");
  		}
  	}
  	else if(pos_x == 2)
  	{
  		if(pos_y == 2)
  		{
        swapTile(matrix,pos_x, pos_y, pos_x-1, pos_y); 
        incrementMove("L");/////
  			swapTile(matrix,pos_x, pos_y, pos_x, pos_y-1);
        incrementMove("U");
  		}
  		else if(pos_y == 1)
  		{ 
        swapTile(matrix,pos_x, pos_y, pos_x-1, pos_y);
        incrementMove("L");  
  			swapTile(matrix,pos_x, pos_y, pos_x, pos_y+1);
        incrementMove("D");
  			swapTile(matrix,pos_x, pos_y, pos_x, pos_y-1);
        incrementMove("U");
  		}
      else if (pos_y == 0)
      {
        swapTile(matrix,pos_x, pos_y, pos_x, pos_y+1);
        incrementMove("D");
        swapTile(matrix,pos_x, pos_y, pos_x-1, pos_y);
        incrementMove("L");
      }
  	}

  	else if(pos_x == 1)
  	{
      if (pos_y == 1)
      {
    		swapTile(matrix,pos_x, pos_y, pos_x, pos_y+1);
        incrementMove("D");
        swapTile(matrix,pos_x, pos_y, pos_x, pos_y-1);
        incrementMove("U");
        swapTile(matrix,pos_x, pos_y, pos_x+1, pos_y);
        incrementMove("R");
  	  	swapTile(matrix,pos_x, pos_y, pos_x-1, pos_y);
        incrementMove("L");
      }	
      else if (pos_y == 0)
      { 
        swapTile(matrix,pos_x, pos_y, pos_x, pos_y+1);
        incrementMove("D");
        swapTile(matrix,pos_x, pos_y, pos_x+1, pos_y);
        incrementMove("R");
        swapTile(matrix,pos_x, pos_y, pos_x-1, pos_y);
        incrementMove("L");
      }
      else if (pos_y == 2)
      {
        
        swapTile(matrix,pos_x, pos_y, pos_x, pos_y-1);
        incrementMove("U");
        swapTile(matrix,pos_x, pos_y, pos_x+1, pos_y);
        incrementMove("R");
        swapTile(matrix,pos_x, pos_y, pos_x-1, pos_y);
        incrementMove("L");
      }
  	}
  	return newConfig;
}

string Board_Tile::swapTile(char toSwap[3][3],int startX, int startY, int endX, int endY)
{
  string newConfig = "";
	int row = 3;
	int col = 3;

	char copy[3][3];

    for (int n = 0; n < row; n++)
  	{
  		for (int m = 0; m < col; m++)
    	{
    		copy[n][m] = toSwap[n][m];	
      }
    }

    char temp = copy[startX][startY];

    copy[startX][startY] = copy[endX][endY];

    copy[endX][endY] = temp;

    for (int n = 0; n < row; n++)
    {
      for (int m = 0; m < col; m++)
      {
        newConfig += copy[n][m];
      }
    }

    this->tempNextMoves.push_back(newConfig);

    return newConfig;
}

tuple<int, int> Board_Tile::getCoordinate(string config, char toFind)
{
  char matrix [3][3];
  int row = 3;
  int col = 3;

  int inc = 0;
  int pos_x, pos_y;

    for (int n = 0; n < row; n++)
    {
      for (int m = 0; m < col; m++)
      {
        matrix[n][m]=config[inc];
        if(config[inc] == toFind)
        {
          pos_y = m;
          pos_x = n;
        }
        inc++;
      }
    }
  return make_tuple(pos_x, pos_y); 
}

void Board_Tile::incrementMove(string s)
{
  this->moves_from_start += s;
}

string Board_Tile::toString() const
{
  return this->config;
}

























