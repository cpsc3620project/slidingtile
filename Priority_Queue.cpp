class Toast
{
public:
    int bread;
    int butter;

    Toast(int bread, int butter)
        : bread(bread), butter(butter)
    {

    }
};

struct ToastCompare
{
    bool operator()(const Toast &t1, const Toast &t2) const
    {
        //int t1value = t1.bread * 1000 + t1.butter;
        //int t2value = t2.bread * 1000 + t2.butter;
        return  t1.bread < t2.bread;
    }
};

#include <iostream>
#include <queue>
#include <vector>

//#include "Toast.h"

using std::priority_queue;
using std::vector;
using std::cout;
using std::endl;

int main(int argc, char ** argv)
{
    Toast toast1(2, 200);
    Toast toast2(1, 30);
    Toast toast3(1, 10);
    Toast toast4(3, 1);
    
    //priority_queue<Toast> queue;
    priority_queue<Toast, vector<Toast>, ToastCompare> queue;

    queue.push(toast1);
    queue.push(toast2);
    queue.push(toast3);
    queue.push(toast4);

    while (!queue.empty())
    {
        Toast t = queue.top();
        cout << "bread " << t.bread << " butter " << t.butter << std::endl;
        queue.pop();
    }

    system("pause");

    return 0;
}