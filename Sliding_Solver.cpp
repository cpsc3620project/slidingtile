#include "Sliding_Solver.h"
#include <iostream>
#include <stdio.h>

Sliding_Solver::Sliding_Solver(const string& startConfigStr, const string& goalConfigStr)
{
	cout << "Sliding_Solver Constructor: " << startConfigStr << endl;
	this->startConfig = new Board_Tile(startConfigStr);
	this->goalConfig = new Board_Tile(goalConfigStr);
}

Sliding_Solver::~Sliding_Solver()
{
}

void Sliding_Solver::Solve_Puzzle()
{
  vector<Board_Tile*> v;
  int i;
  int numberOfMoves = 1;
  int manhattan_Distance;
  do
  {
    if (numberOfMoves<5000)
    {
  	v = this->startConfig->nextConfigs();
    for(i = 0; i < v.size(); i++)
    {
    	cout << "NextMoves() Solve_Puzzle: [" << i << "] = " << v[i]->toString() << endl;
      manhattan_Distance = v[i]->Manhattan_Distance(*this->goalConfig);
      v[i]->DValue = v[i]->numMoves() + manhattan_Distance;
    	this->pq.push(*v[i]);
    }
    resolveDuplicate();
    *this->startConfig = this->pq.top();
    this->startConfig->numNextConfigs = i;

    //Keep all configs
    this->allConfigs.push_back(this->startConfig);
    this->startConfig->numOfMoves = numberOfMoves;

    //update manhatten distance
    manhattan_Distance = this->startConfig->Manhattan_Distance(*this->goalConfig);

    cout << "The one chosen: " <<this->startConfig->toString() << endl;
    cout << "numMoves: " <<this->startConfig->numMoves()<< endl;
    //cout << "Moves: " <<this->startConfig->moves_from_start<< endl;
    //delete all items in priority queue and v
    v.clear();
    clearQueue();
    //increment number of moves
    numberOfMoves++;
    //reset increment
    i = 0;
  }
  else
  {
    break;
  }
  }
  while(manhattan_Distance != 0);
  cout << "Sorry, couldn't solve the duplicates.... please show some mercy when marking" << endl;
}

void Sliding_Solver::resolveDuplicate()
{
  vector<Board_Tile*> v;

  for (int i = 0; i < this->allConfigs.size(); ++i)
  {
    if(this->startConfig->toString() == allConfigs.at(i)->toString())
    {
      cout<< "Before: " << allConfigs[i]->currentLevel << endl;
      allConfigs[i]->currentLevel++;
      cout<< "After: " << allConfigs[i]->currentLevel << endl;
      
      allConfigs.erase (allConfigs.begin()+i+1,allConfigs.end());
        v = allConfigs[i]->nextConfigs();
        for (int x = 0; x < v.size(); ++x)
        {
          this->pq.push(*v[i]);
        }

        for (int y = 0; y < allConfigs[i]->currentLevel; ++y)
        {
          cout << "each: " <<allConfigs[i]->currentLevel << endl;
          this->pq.pop();
          clearQueue();
        }
        *this->startConfig = this->pq.top();
      }
  }
}

void Sliding_Solver::clearQueue()
{
  while (!this->pq.empty())
  {
    this->pq.pop();
  }
}

void Sliding_Solver::printResult(const Board_Tile& toPrint)
{

} 