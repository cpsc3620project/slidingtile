#ifndef BOARD_TILE_H_
#define BOARD_TILE_H_

#include <tuple>
#include <vector>
#include <string>
#include <stdlib.h>  

using namespace std;

class Board_Tile {
public:
	int numOfMoves = 0;
	Board_Tile(const string&);
	~Board_Tile();
	vector<Board_Tile*> nextConfigs();
	int numMoves();
	int Manhattan_Distance(const Board_Tile& goalConfig);
	string toString() const;
	int DValue;
	string moves_from_start;
	int numNextConfigs = 0;
	int currentLevel = 0;
private:
	string config;
	vector<string> tempNextMoves;
	string getNewConfig(string startConfig);
	tuple<int, int> getCoordinate(string config, char toFind);
	string swapTile(char toSwap[3][3], int startX, int startY, int endX, int endY);
	void incrementMove(string s);
};

#endif /* BOARD_TILE_H_ */
