#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "Board_Tile.h"
#include "Sliding_Solver.h"

using namespace std;

int main(void) {
	string startConfig = "", goalConfig = "";
	cout << "Enter start configuration:" << endl;
  cin >> startConfig;
  cout << "Enter goal configuration:" << endl;
  cin >> goalConfig;
  cout << "Goal: " << goalConfig << endl;

  Sliding_Solver* solver = new Sliding_Solver(startConfig,goalConfig);
  solver->Solve_Puzzle();

	return EXIT_SUCCESS;
}
