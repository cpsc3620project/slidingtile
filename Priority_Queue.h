#ifndef PRIORITY_QUEUE_
#define PRIORITY_QUEUE_

#include <string>
#include <stdlib.h>

using namespace std;

class Priority_Queue {
public:
	Priority_Queue();
	~Priority_Queue();
	void Solve_Puzzle();
	void printResult();

private:
	maxHeapify
};

#endif /* PRIORITY_QUEUE_ */

void maxHeapify(int *a, int i, int len);
void buildMaxHeap(int *a,int size);void print(int *a,int size);
int heapExtractMax(int *a,int &heapsize);
void heapIncreaseKey(int *a,int i,int key);
void insert(int *a,int key,int &heapsize);
